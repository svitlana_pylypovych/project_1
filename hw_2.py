n = int(input()) // 2
s, num = 0, 1
while num <= n:
    num <<= 1
    s += 1
print(f'{2} ** {s} = {num}')